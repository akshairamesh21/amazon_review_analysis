
## MAPREDUCE

* [1.](/src/main/java/mapreduce/job1/AmazonFoodAnalytic.java) A job that is able to generate, for each year,
the ten most used words in the reviews (summary field) in order of frequency, indicating, for each word,
its frequency, or the number of occurrences of the word in reviews that year.

* [2.](/src/main/java/mapreduce/job2/AmazonFoodAnalytic.java) A job that can generate, for each product,
the average score obtained in each of the years between 2003 and 2012, indicating ProductId followed by all the average scores obtained
in the interval years. The result must be ordered based on the ProductId.

* [3.](/src/main/java/mapreduce/job3/AmazonFoodAnalytic.java) A job capable of generating pairs of products that have at least one user in common,
or that have been reviewed by the same user, indicating, for each pair, the number of users in common.
The result must be ordered based on the ProductId of the first element of the pair and, possibly, must not have duplicates.

## RESULTS
The output files after executing all the 3 jobs, along with the corresponding screenshots can be found in the Results folder from the current directory.

## INSTRUCTION TO EXECUTE
1. Run gradle build to download the project dependencies.
2. Create the jars for the jobs with the command gradle fatJar1, gradle fatJar2 and gradle fatJar3.
3. Launch Hadoop and copy the csv of the unpacked dataset to hdfs in the input folder.
4. Create the hadoop output folder where to allocate the result file
5. Execute any project job,using the below command by replacing * with the job number tat we want to execute.

## EXECUTING THE JOBS: 
Command for executing any of jobs(1,2,3)
$HADOOP_HOME/bin/hadoop jar ~/mapreduce_files/build/libs/AmazonFoodAnalytic*-all-1.0.0.jar input/Reviews.csv output/result


